const multer = require('multer');
const path = require('path');
const fs = require('fs');
const cloudinary = require('cloudinary').v2;

const ACCEPTED_FILES = ['image/jpg', 'image/jpeg', 'image/png'];

const fileFilter = (req, file, cb) => {
    if (!ACCEPTED_FILES.includes(file.mimetype)) {
        const error = new Error('Format not supported');
        error.status = 400;
        cb(error);
    }
    cb(null, true);
};

const storage = multer.diskStorage({
    filename: (req, file, cb) => {
        const name = `${Date.now()}-${file.originalname}`;
        cb(null, name);
    },
    destination: (req, file, cb) => {
       const dir = path.join(__dirname, '../public/uploads');
       cb(null, dir);
    },
});

const upload = multer({ storage, fileFilter });

const uploadToCloudinary = async (req, res, next) => {
    if (req.file) {
        const filePath = req.file.path;
        const img = await cloudinary.uploader.upload(filePath);
        req.imageUrl = img.secure_url;
        await fs.unlinkSync(filePath);
        return next();
    }
    return next();
}

module.exports = {
    upload,
    uploadToCloudinary,
};