const isAuth = (req, res, next) => req.isAuthenticated()
    ? next() 
    : res.status(401).json('Not authorised');

const isAdmin = (req, res, next) => {
    if(req.isAuthenticated()) {
        if(req.user.role === "admin") {
            return next();
        }
        return res.status(403).json('Not authorised');
    } else {
        return res.status(401).json('Not authorised');
    };
};

module.exports = { isAuth, isAdmin };