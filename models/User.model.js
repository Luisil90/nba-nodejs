const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        email: { type: String, required: true },
        role: { type: String, required: true, enum: ['user', 'admin'], default: 'user' },
        password: { type: String, required: true },
        name: { type: String, required: true },
        avatar: { type: String, required: true, default: "https://everydaynutrition.co.uk/wp-content/uploads/2015/01/default-user-avatar-300x300.png" },
    },
    { timestamps: true }
);

const User = mongoose.model('User', userSchema);

module.exports = User;