const mongoose = require('mongoose');

const { Schema } = mongoose;

const teamSchema = new Schema (
    {
        name: { type: String, required: true},
        state: { type: String, required: true},
        city: { type: String, required: true},
        population: { type: Number},
        image: { type: String, required: true},
    },

    { timestamps: true }
);


//Aqui pasamos a crear el modelos

const Team = mongoose.model('Teams', teamSchema);

module.exports = Team;
