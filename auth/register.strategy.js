const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require('bcrypt');
const User = require('../models/User.model');
const { isValidEmail, isValidPassword, throwError } = require("./utils");

const registerStrategy = new LocalStrategy(
  {
    usernameField: "email",
    passwordField: "password",
    passReqToCallback: true,
  },
  async (req, email, pass, done) => {
    const existingUser = await User.findOne({ email });

    if (existingUser) throwError(400, 'User already in use', done);

    if (!isValidEmail(email)) throwError(400, 'Invalid email adress', done);

    if (!isValidPassword(pass)) throwError(400, 'Password must have between 6-20 char 1 Upper and 1 lower case', done);

    const saltRounds = 10;
    const hash = await bcrypt.hash(pass, saltRounds);

    const newUser = new User({
        email,
        role: 'user',
        password: hash,
        name: req.body.name,
    });

    const user = await newUser.save();
    user.password = null;
    return done(null, user);
  }
);

module.exports = registerStrategy;
