const express = require("express");
const Player = require("../models/Player.model");
const controller = require("../controllers/players.controller");
const {
  upload,
  uploadToCloudinary,
} = require("../middlewares/file.middlewares");

const router = express.Router();

router.get("/", controller.indexGet);

router.post(
  "/create",
  [upload.single("image"), uploadToCloudinary],
  controller.createPost );

module.exports = router;
