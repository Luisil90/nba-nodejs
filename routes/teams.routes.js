const express = require('express');
const Team = require('../models/Team.model');

const router = express.Router();

router.get('/', async (req, res, next) => {
    console.log('teams');
    try {
        const teams =  await Team.find();
        return res.json(teams);

    } catch (error) {
        return next(error);
    }
});

module.exports = router;