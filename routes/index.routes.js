const express = require('express');
const User = require('../models/User.model');

const router = express.Router();

router.get('/', (req, res, next) => {
    return res.status(200).render('index',{title: 'Home NBA Website Project for Upgrade'});
});

router.get('/save-user', async (req, res, next) => {
    try {
        const newUser = new User({
            name: 'Gerbasio Alberto',
            password: '1234asdf',
            email: 'GA@hub.com',
        });
    
        const user = await newUser.save();

        console.log(user);
    
        return res.json(user);
    } catch (error) {
        console.log('error ', error);
    }
});



module.exports = router;