const Player = require('../models/Player.model')
const indexGet = async (req, res, next) => {
  try {
    const players = await Player.find();
    return res.json(players);
  } catch (error) {

    return next(error);
  }
};

const createPost = async (req, res, next) => {

  const { name, number, team, isAllStar } = req.body;

  const newPlayer = new Player({
    name,
    number,
    team,
    isAllStar,
  });

  const player = await newPlayer.save();

  console.log(player);

  return res.json(player);
};

module.exports = { indexGet, createPost };
