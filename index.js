const express = require("express");
const path = require('path');
const passport = require("passport");
const session = require('express-session');
const MongoStore = require('connect-mongo');
const http = require('http');
const hbs = require('hbs');



//DOTENV para las variables de entorno
const dotenv = require("dotenv");
dotenv.config();

const db = require("./config/db.config");
db.connect();

const auth = require("./auth");
auth.useStrategies();



const PORT = process.env.PORT || 3000;

const indexRoutes = require("./routes/index.routes");
const authRoutes = require("./routes/auth.routes");
const teamsRoutes = require("./routes/teams.routes");
const playersRoutes = require("./routes/players.routes");



const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');


app.use(express.static(path.join(__dirname, 'public')));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.use(passport.initialize());
app.use(passport.session());

app.use('/', indexRoutes);
app.use('/auth', authRoutes);
app.use('/teams', teamsRoutes);
app.use('/players', playersRoutes);

app.use("*", (req, res, next) => {
  const error = new Error("Route not found");
  error.status = 404;
  return res.json(error.message);
});

app.use((error, req, res, next) => {
  return res.json({
    message: error.message || "Unexpected Error",
    status: error.status || 500,
  });
});

app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}`));
